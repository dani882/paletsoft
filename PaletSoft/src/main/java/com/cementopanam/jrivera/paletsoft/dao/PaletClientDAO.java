package com.cementopanam.jrivera.paletsoft.dao;

import java.util.List;

import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import com.cementopanam.jrivera.paletsoft.entidad.Palet;
import com.cementopanam.jrivera.paletsoft.util.HibernateUtil;

public class PaletClientDAO extends OperacionPaletDAO {

    private static final Logger LOGGER = LogManager.getLogger(PaletClientDAO.class);

    /*
     * Search for all Palets
     * 
     * @see com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl#findAll()
     * 
     * @return List all palets
     */
    
    
    @SuppressWarnings("unchecked")
    public List<Palet> findAllClients() {

        List<Palet> list = null;
        Session session = getOpenSession();
        // Criteria criteria = null;

        try {

            /*
             * SELECT o.nro_entrega_sap AS 'Numero de Entrega SAP',
             * c.nombre_cliente AS 'Cliente', o.estatus AS 'Estatus' FROM
             * palet_db.tbloperacion_palet o INNER JOIN palet_db.tblcliente c ON
             * o.codigo_cliente = c.codigo_cliente WHERE o.estatus = 'Pendiente
             * de Devolver';
             */

            Query query = session.createQuery("SELECT op.nroEntregaSap, c.nombreCliente "
                    + "FROM OperacionPalet op, Cliente c where op.codigoCliente = c.codigoCliente");

            list = query.getResultList();
        }

        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            session.close();
        }

        HibernateUtil.shutdown();
        return list;
    }
}
