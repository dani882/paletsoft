package com.cementopanam.jrivera.paletsoft.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

public class CambioEstatusGUI extends JInternalFrame {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JTable tblPendienteCambio;
    private JTextField txtNumeroSAP;
    private JTextField txtCantidadDespachada;
    private JTextField txtCantidadRecibida;
    private JTextField txtDiferencia;
    private JButton btnCancelar;
    private JButton btnGuardar;

    /**
     * Create the frame.
     */
    public CambioEstatusGUI() {
        setTitle("Cambio de Estatus");
        setBounds(100, 100, 531, 411);
        getContentPane().setLayout(null);

        JPanel panelPendienteCambio = new JPanel();
        panelPendienteCambio.setBorder(
                new TitledBorder(null, "Pendiente de Cambio", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        panelPendienteCambio.setBounds(10, 11, 495, 174);
        getContentPane().add(panelPendienteCambio);
        panelPendienteCambio.setLayout(new BorderLayout(0, 0));

        JScrollPane scrollPane = new JScrollPane();
        panelPendienteCambio.add(scrollPane);

        tblPendienteCambio = new JTable();
        //TODO Poblar datos Buscar Info
        tblPendienteCambio.setModel(new DefaultTableModel(
            new Object[][] {
            },
            new String[] {
                "Numero de Entrega SAP", "Cliente", "Estatus"
            }
        ));
        tblPendienteCambio.setForeground(Color.GRAY);
        scrollPane.setViewportView(tblPendienteCambio);
        tblPendienteCambio.setFont(new Font("Verdana", Font.PLAIN, 12));

        JLabel lblNumeroDeEntrega = new JLabel("Numero de Entrega SAP");
        lblNumeroDeEntrega.setFont(new Font("SansSerif", Font.PLAIN, 12));
        lblNumeroDeEntrega.setBounds(10, 209, 152, 21);
        getContentPane().add(lblNumeroDeEntrega);

        JLabel lblCantidadDespachada = new JLabel("Cantidad Despachada");
        lblCantidadDespachada.setFont(new Font("SansSerif", Font.PLAIN, 12));
        lblCantidadDespachada.setBounds(10, 241, 152, 21);
        getContentPane().add(lblCantidadDespachada);

        JLabel lblCantidadRecibidaParcialmente = new JLabel("Cantidad Recibida Parcialmente");
        lblCantidadRecibidaParcialmente.setFont(new Font("SansSerif", Font.PLAIN, 12));
        lblCantidadRecibidaParcialmente.setBounds(10, 273, 187, 21);
        getContentPane().add(lblCantidadRecibidaParcialmente);

        JLabel lblDiferencia = new JLabel("Diferencia");
        lblDiferencia.setFont(new Font("SansSerif", Font.PLAIN, 12));
        lblDiferencia.setBounds(10, 305, 152, 21);
        getContentPane().add(lblDiferencia);

        btnCancelar = new JButton("Cancelar");
        btnCancelar.setFont(new Font("SansSerif", Font.PLAIN, 12));
        btnCancelar.setBounds(416, 341, 89, 28);
        getContentPane().add(btnCancelar);

        btnGuardar = new JButton("Guardar");
        btnGuardar.setFont(new Font("SansSerif", Font.PLAIN, 12));
        btnGuardar.setBounds(317, 342, 89, 28);
        getContentPane().add(btnGuardar);

        txtNumeroSAP = new JTextField();
        txtNumeroSAP.setFont(new Font("Verdana", Font.PLAIN, 12));
        txtNumeroSAP.setBounds(219, 210, 104, 23);
        getContentPane().add(txtNumeroSAP);
        txtNumeroSAP.setColumns(10);

        txtCantidadDespachada = new JTextField();
        txtCantidadDespachada.setEnabled(false);
        txtCantidadDespachada.setEditable(false);
        txtCantidadDespachada.setFont(new Font("Verdana", Font.PLAIN, 12));
        txtCantidadDespachada.setColumns(10);
        txtCantidadDespachada.setBounds(219, 242, 104, 23);
        getContentPane().add(txtCantidadDespachada);

        txtCantidadRecibida = new JTextField();
        txtCantidadRecibida.setEnabled(false);
        txtCantidadRecibida.setEditable(false);
        txtCantidadRecibida.setFont(new Font("Verdana", Font.PLAIN, 12));
        txtCantidadRecibida.setColumns(10);
        txtCantidadRecibida.setBounds(219, 274, 104, 23);
        getContentPane().add(txtCantidadRecibida);

        txtDiferencia = new JTextField();
        txtDiferencia.setEnabled(false);
        txtDiferencia.setEditable(false);
        txtDiferencia.setFont(new Font("Verdana", Font.PLAIN, 12));
        txtDiferencia.setColumns(10);
        txtDiferencia.setBounds(219, 306, 104, 23);
        getContentPane().add(txtDiferencia);

    }
}