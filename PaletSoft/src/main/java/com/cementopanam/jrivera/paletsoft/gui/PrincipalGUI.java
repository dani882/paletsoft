package com.cementopanam.jrivera.paletsoft.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;

import com.cementopanam.jrivera.paletsoft.main.Autenticacion;

/**
 * Ventana principal para elegir la accion deseada
 * 
 * @author jrivera
 *
 */
public class PrincipalGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4178636256039538400L;
	private JPanel contentPane;
	private JDesktopPane desktopPanePrincipal;

	private Dimension dim;
	private int w, h, x, y;

	/**
	 * Create the frame.
	 */
	public PrincipalGUI() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(PrincipalGUI.class.getResource("/iconos/logo.png")));

		setExtendedState(Frame.MAXIMIZED_BOTH);

		// Obtiene el tamaño de la pantalla
		dim = Toolkit.getDefaultToolkit().getScreenSize();

		// Determina la nueva localizacion de la ventana
		w = getSize().width;
		h = getSize().height;
		x = (dim.width - w) / 2;
		y = (dim.height - h) / 2;

		// Centra la ventana
		setLocation(x, y);
		setTitle("PaletSoft");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1024, 768);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);

		JMenuItem mntmCerrarSesion = new JMenuItem("Cerrar Sesion");
		mntmCerrarSesion.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {

				Autenticacion aut = new Autenticacion();
				aut.setVisible(true);

				dispose();
			}
		});
		mnArchivo.add(mntmCerrarSesion);

		JSeparator separator = new JSeparator();
		mnArchivo.add(separator);

		JMenuItem mntmSalir = new JMenuItem("Salir");
		mntmSalir.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {

				System.exit(0);
			}
		});
		mnArchivo.add(mntmSalir);

		JMenu mnOperacion = new JMenu("Operacion");
		menuBar.add(mnOperacion);

		JMenuItem mntmCambioDeEstatus = new JMenuItem("Cambio de Estatus");
		mntmCambioDeEstatus.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {

				CambioEstatusGUI cambioEstatusGUI = new CambioEstatusGUI();
				desktopPanePrincipal.add(cambioEstatusGUI);
				cambioEstatusGUI.setVisible(true);
			}
		});
		mnOperacion.add(mntmCambioDeEstatus);

		JMenuItem mntmDevolucionPalet = new JMenuItem("Devolucion Palet");
		mntmDevolucionPalet.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {

				DevolucionPaletGUI devolucionPaletGUI = new DevolucionPaletGUI();
				desktopPanePrincipal.add(devolucionPaletGUI);
				devolucionPaletGUI.setVisible(true);
			}
		});
		
		JSeparator separator_8 = new JSeparator();
		mnOperacion.add(separator_8);
		mnOperacion.add(mntmDevolucionPalet);

		JMenuItem mntmOperacionPalet = new JMenuItem("Operacion Palet");
		mntmOperacionPalet.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {

				OperacionPaletGUI operacionPaletGUI = new OperacionPaletGUI();
				desktopPanePrincipal.add(operacionPaletGUI);
				operacionPaletGUI.setVisible(true);
			}
		});
		
		JSeparator separator_9 = new JSeparator();
		mnOperacion.add(separator_9);
		mnOperacion.add(mntmOperacionPalet);

		JSeparator separator_1 = new JSeparator();
		mnOperacion.add(separator_1);

		JMenuItem mntmImprimir = new JMenuItem("Imprimir");
		mnOperacion.add(mntmImprimir);

		JMenu mnRegistro = new JMenu("Registro");
		menuBar.add(mnRegistro);

		JMenuItem mntmCliente = new JMenuItem("Cliente");
		mntmCliente.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {

				ClienteGUI clienteGUI = new ClienteGUI();
				desktopPanePrincipal.add(clienteGUI);
				clienteGUI.setVisible(true);

			}
		});
		mnRegistro.add(mntmCliente);

		JSeparator separator_4 = new JSeparator();
		mnRegistro.add(separator_4);

		JMenuItem mntmChofer = new JMenuItem("Chofer");
		mntmChofer.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {

				ChoferGUI chofer = new ChoferGUI();
				desktopPanePrincipal.add(chofer);
				chofer.setVisible(true);
			}
		});
		mnRegistro.add(mntmChofer);

		JSeparator separator_5 = new JSeparator();
		mnRegistro.add(separator_5);

		JMenuItem mntmPalet = new JMenuItem("Palet");
		mntmPalet.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {

				PaletGUI paletGUI = new PaletGUI();
				desktopPanePrincipal.add(paletGUI);
				paletGUI.setVisible(true);
			}
		});
		mnRegistro.add(mntmPalet);

		JMenu mnReporte = new JMenu("Reporte");
		menuBar.add(mnReporte);

		JMenu mnPalet = new JMenu("Palet");
		mnReporte.add(mnPalet);

		JMenuItem mntmInformeGeneral = new JMenuItem("Informe General");
		mntmInformeGeneral.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {

				ReporteGeneralGUI reporteGeneralGUI = new ReporteGeneralGUI();
				desktopPanePrincipal.add(reporteGeneralGUI);
				reporteGeneralGUI.setVisible(true);
			}
		});
		mnPalet.add(mntmInformeGeneral);

		JMenuItem mntmDespachada = new JMenuItem("Despachada");
		mnPalet.add(mntmDespachada);

		JMenuItem mntmRetornada = new JMenuItem("Retornada");
		mnPalet.add(mntmRetornada);

		JSeparator separator_2 = new JSeparator();
		mnReporte.add(separator_2);

		JMenuItem mntmReporteCliente = new JMenuItem("Cliente");
		mnReporte.add(mntmReporteCliente);

		JSeparator separator_3 = new JSeparator();
		mnReporte.add(separator_3);

		JMenuItem mntmChofer_1 = new JMenuItem("Chofer");
		mnReporte.add(mntmChofer_1);

		JMenu mnConfiguracion = new JMenu("Configuracion");
		menuBar.add(mnConfiguracion);

		JMenuItem mntmAcercaDePaletsoft = new JMenuItem("Acerca de PaletSoft");
		mnConfiguracion.add(mntmAcercaDePaletsoft);

		JSeparator separator_7 = new JSeparator();
		mnConfiguracion.add(separator_7);

		JMenuItem mntmCambioDeContrasea = new JMenuItem("Cambio de Contraseña");
		mntmCambioDeContrasea.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {

				CambiarContrasenaGUI contrasena = new CambiarContrasenaGUI();
				desktopPanePrincipal.add(contrasena);
				contrasena.setVisible(true);

			}
		});
		mnConfiguracion.add(mntmCambioDeContrasea);

		JSeparator separator_6 = new JSeparator();
		mnConfiguracion.add(separator_6);

		JMenuItem mntmUsuario = new JMenuItem("Usuario");
		mnConfiguracion.add(mntmUsuario);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		desktopPanePrincipal = new JDesktopPane();
		desktopPanePrincipal.setBackground(Color.GRAY);
		contentPane.add(desktopPanePrincipal, BorderLayout.CENTER);
	}
}
