package com.cementopanam.jrivera.paletsoft.dao;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;

import com.cementopanam.jrivera.paletsoft.entidad.OperacionPalet;
import com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl;
import com.cementopanam.jrivera.paletsoft.util.HibernateUtil;

/**
 * Home object for domain model class OperacionPaletGUI.
 * 
 * @see .OperacionPalet
 * @author Jesus Rivera
 */
public class OperacionPaletDAO implements PersistenceCRUDImpl<OperacionPalet, Integer> {

    private static final Logger LOGGER = LogManager.getLogger(OperacionPaletDAO.class);
    final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public Session getOpenSession() {
        return this.sessionFactory.openSession();
    }
    
    /*
     * Save the object in the database
     * 
     * @see
     * com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl#save(java.lang.
     * Object)
     */
    @Override
    public void save(OperacionPalet entity) {

        LOGGER.info("persisting OperacionPaletGUI instance");

        try (Session session = sessionFactory.openSession();) {

            session.getTransaction().begin();
            session.save(entity);
            session.getTransaction().commit();

        } catch (HibernateException hbe) {
            LOGGER.error(hbe.getMessage(), hbe);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Override
    public void update(OperacionPalet entity) {

        LOGGER.info("updating Cliente instance");

        try (Session session = sessionFactory.openSession();) {

            session.getTransaction().begin();
            session.update(entity);
            session.getTransaction().commit();

        } catch (HibernateException hbe) {
            LOGGER.error(hbe.getMessage(), hbe);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Override
    public OperacionPalet findById(Integer id) {

        Session session = sessionFactory.getCurrentSession();
        OperacionPalet operacionPalet = session.get(OperacionPalet.class, id);
        session.close();

        return operacionPalet;
    }

    @Override
    public void delete(OperacionPalet entity) {

        LOGGER.info("deleting OperacionPaletGUI instance");

        try (Session session = sessionFactory.openSession();) {

            OperacionPalet operacionPalet = findById(entity.getCodigoOperacionPalet());
            // TODO Modificar para borrar mediante otros parametros ya que solo
            // borra por codigo
            session.getTransaction().begin();
            session.delete(operacionPalet);
            session.getTransaction().commit();
        }

    }

    /*
     * Search for all Palets
     * 
     * @see com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl#findAll()
     * 
     * @return List all palets
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<OperacionPalet> findAll() {

        List<OperacionPalet> palets = null;
        Session session = null;
        DetachedCriteria criteria = null;

        try {
            session = sessionFactory.openSession();

            criteria = DetachedCriteria.forClass(OperacionPalet.class);
            palets = criteria.getExecutableCriteria(session).list();

        }

        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            session.close();
        }

        return palets;
    }

    /**
     * Search for all Palets
     * 
     * @return the list palets by status
     */
    @SuppressWarnings({ "unchecked", "deprecation" })
    public List<OperacionPalet> findByStatus() {

        // TODO Terminar esta operacion
        List<OperacionPalet> list = null;
        Session session = null;
        // DetachedCriteria criteria = null;

        try {
            session = sessionFactory.openSession();
            Criteria criteriaOperacion = session.createCriteria(OperacionPalet.class);
            // Criteria criteriaCliente =
            // criteriaOperacion.createCriteria("Cliente");
            // criteriaCliente.add(Restrictions.like("estatus", "Pendiente de
            // Devolver"));
            list = criteriaOperacion.list();
            // list = criteria.getExecutableCriteria(session).list();

        }

        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            session.close();
        }

        return list;
    }
    
    

}
