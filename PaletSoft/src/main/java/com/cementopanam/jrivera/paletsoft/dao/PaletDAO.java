package com.cementopanam.jrivera.paletsoft.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.cementopanam.jrivera.paletsoft.entidad.InventarioPalet;
import com.cementopanam.jrivera.paletsoft.entidad.Palet;
import com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl;
import com.cementopanam.jrivera.paletsoft.util.HibernateUtil;

/**
 * Home object for domain model class Palet.
 * 
 * @see .Palet
 * @author Jesus Rivera
 */
public class PaletDAO implements PersistenceCRUDImpl<Palet, Integer> {

    private static final Logger LOGGER = LogManager.getLogger(PaletDAO.class);
    private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public void save(Palet palet) {

        LOGGER.info("persisting Palet instance");

        try (Session session = sessionFactory.openSession();) {

            session.getTransaction().begin();
            session.save(palet);
            session.getTransaction().commit();

        } catch (HibernateException hbe) {
            LOGGER.error(hbe.getMessage(), hbe);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Override
    public void update(Palet palet) {

        LOGGER.info("updating Sindicato instance");

        try (Session session = sessionFactory.openSession();) {

            session.getTransaction().begin();
            session.update(palet);
            session.getTransaction().commit();

        } catch (HibernateException hbe) {
            LOGGER.error(hbe.getMessage(), hbe);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Override
    public Palet findById(Integer id) {

        Session session = sessionFactory.openSession();
        Palet palet = session.get(Palet.class, id);
        return palet;
    }

    @Override
    public void delete(Palet palet) {

        LOGGER.info("deleting Palet entity instance");

        try (Session session = sessionFactory.openSession();) {

            Palet pal = findById(palet.getCodigoPalet());
            // TODO Modificar para borrar mediante otros parametros ya que solo
            // borra por codigo
            session.getTransaction().begin();
            session.delete(pal);
            session.getTransaction().commit();

        }

    }

    @Override
    public List<Palet> findAll() {

        List<Palet> palets = null;
        Session session = null;
        CriteriaBuilder builder = null;
        CriteriaQuery<Palet> criteria = null;

        try {
            session = sessionFactory.openSession();
            // Create CriteriaBuilder
            builder = session.getCriteriaBuilder();
            // Create CriteriaQuery
            criteria = builder.createQuery(Palet.class);
            // Specify criteria root
            criteria.from(Palet.class);
            // Execute query
            palets = session.createQuery(criteria).getResultList();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            session.close();
        }

        return palets;
    }

    /**
     * Find the Palet ID by name description
     * 
     * @param name
     *            - name of the palet
     * @see http://docs.oracle.com/javaee/6/tutorial/doc/gjivm.html
     * @return the Palet ID
     */
    public Integer findPaletID(String name) {

        Palet palet = null;
        Session session = null;

        try {
            session = sessionFactory.openSession();
            // Create CriteriaBuilder
            CriteriaBuilder builder = session.getCriteriaBuilder();
            // Create CriteriaQuery
            CriteriaQuery<Palet> criteriaQuery = builder.createQuery(Palet.class);
            // Specify criteria root
            Root<Palet> rootPalet = criteriaQuery.from(Palet.class);
            // Specify parameter to find
            criteriaQuery.where(builder.like(rootPalet.get("descripcion"), name));

            palet = session.createQuery(criteriaQuery).getSingleResult();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            session.close();
        }

        return palet.getCodigoPalet();

    }
}