package com.cementopanam.jrivera.paletsoft.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cementopanam.jrivera.paletsoft.dao.ClienteDAO;
import com.cementopanam.jrivera.paletsoft.entidad.Cliente;
import com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl;

public class ClienteGUI extends JInternalFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 5102289983577498011L;
    private static final Logger LOGGER = LogManager.getLogger(ClienteGUI.class);
    private JTextField txtCodCliente;
    private JTextField txtNombreCliente;
    private JComboBox<Object> cbTipoCliente;
    private JCheckBox chEstatus;

    /**
     * Create the dialog.
     */
    public ClienteGUI() {
        setIconifiable(true);
        setClosable(true);
        setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        setTitle("Cliente");
        setFont(new Font("Verdana", Font.PLAIN, 12));
        setBounds(100, 100, 430, 225);
        getContentPane().setLayout(new BorderLayout());
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("Guardar");
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        saveCliente();

                    }
                });
                okButton.setFont(new Font("Verdana", Font.PLAIN, 12));
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
            {
                JButton cancelButton = new JButton("Salir");
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        dispose();
                    }
                });
                cancelButton.setFont(new Font("Verdana", Font.PLAIN, 12));
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
            }
            {
                JLabel label = new JLabel("");
                buttonPane.add(label);
            }
        }
        {
            JPanel panelCentro = new JPanel();
            panelCentro.setBorder(new LineBorder(new Color(0, 0, 0)));
            getContentPane().add(panelCentro, BorderLayout.CENTER);
            panelCentro.setLayout(null);
            {
                JLabel lblCodigoDeCliente = new JLabel("Codigo de Cliente");
                lblCodigoDeCliente.setFont(new Font("Verdana", Font.PLAIN, 12));
                lblCodigoDeCliente.setBounds(10, 11, 122, 22);
                panelCentro.add(lblCodigoDeCliente);
            }
            {
                JLabel lblNombreDelCliente = new JLabel("Nombre del Cliente");
                lblNombreDelCliente.setFont(new Font("Verdana", Font.PLAIN, 12));
                lblNombreDelCliente.setBounds(10, 40, 122, 22);
                panelCentro.add(lblNombreDelCliente);
            }
            {
                JLabel lblTipoDeCliente = new JLabel("Tipo de Cliente");
                lblTipoDeCliente.setFont(new Font("Verdana", Font.PLAIN, 12));
                lblTipoDeCliente.setBounds(10, 68, 122, 22);
                panelCentro.add(lblTipoDeCliente);
            }
            {
                JLabel lblEstatus = new JLabel("Estatus");
                lblEstatus.setFont(new Font("Verdana", Font.PLAIN, 12));
                lblEstatus.setBounds(10, 98, 122, 14);
                panelCentro.add(lblEstatus);
            }
            {
                txtCodCliente = new JTextField();
                txtCodCliente.setFont(new Font("Verdana", Font.PLAIN, 12));
                txtCodCliente.setBounds(147, 9, 137, 24);
                panelCentro.add(txtCodCliente);
                txtCodCliente.setColumns(10);
            }
            {
                cbTipoCliente = new JComboBox<Object>();
                cbTipoCliente.setModel(new DefaultComboBoxModel<Object>(new String[] { "Interno", "Externo" }));
                cbTipoCliente.setFont(new Font("Verdana", Font.PLAIN, 12));
                cbTipoCliente.setBounds(148, 66, 136, 24);
                panelCentro.add(cbTipoCliente);
            }
            {
                txtNombreCliente = new JTextField();
                txtNombreCliente.setFont(new Font("Verdana", Font.PLAIN, 12));
                txtNombreCliente.setColumns(10);
                txtNombreCliente.setBounds(147, 38, 193, 24);
                panelCentro.add(txtNombreCliente);
            }
            {
                chEstatus = new JCheckBox("Activo");
                chEstatus.setSelected(true);
                chEstatus.setFont(new Font("Verdana", Font.PLAIN, 12));
                chEstatus.setBounds(147, 95, 97, 23);
                panelCentro.add(chEstatus);
            }
        }
        {
            JPanel panelNorte = new JPanel();
            getContentPane().add(panelNorte, BorderLayout.NORTH);
        }
        {
            JPanel panelOeste = new JPanel();
            getContentPane().add(panelOeste, BorderLayout.WEST);
        }
        {
            JPanel panelEste = new JPanel();
            getContentPane().add(panelEste, BorderLayout.EAST);
        }
    }

    private void saveCliente() {

        // String codigoCliente = txtCodCliente.getText();
        String nombreCliente = txtNombreCliente.getText().toUpperCase();
        String tipoCliente = String.valueOf(cbTipoCliente.getSelectedItem());

        String estatusCliente = "Inactivo";

        if (chEstatus.isSelected()) {
            estatusCliente = "Activo";
        }

        // Check for empty fields
        if (nombreCliente.trim().equals("") || cbTipoCliente.getSelectedIndex() < 0) {
            JOptionPane.showMessageDialog(null, "Debe rellenar todos los campos");
            return;
        }
        PersistenceCRUDImpl<Cliente, Integer> clienteDAO = new ClienteDAO();
        Cliente cliente = new Cliente();

        cliente.setNombreCliente(nombreCliente);
        cliente.setTipoCliente(tipoCliente);
        cliente.setCodigoUsuario(0);
        cliente.setEstatus(estatusCliente);

        clienteDAO.save(cliente);
    }
}
