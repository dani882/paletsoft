package com.cementopanam.jrivera.paletsoft.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cementopanam.jrivera.paletsoft.gui.PrincipalGUI;

/**
 * Interfaz de Usuario
 * 
 * @author Jesus Rivera
 *
 */
public class Autenticacion extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1810764468227208223L;
	private static final Logger LOGGER = LogManager.getLogger(Autenticacion.class);
	private JPanel contentPane;
	private JTextField txtNombreUsuario;
	private JTextField txtClave;

	private Dimension dim;
	private int w, h, x, y;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, fall back to cross-platform
			try {
				UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			} catch (Exception ex) {
				// not worth my time
			}
		}

		EventQueue.invokeLater(new Runnable() {
			@Override
            public void run() {
				try {
					Autenticacion frame = new Autenticacion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Autenticacion() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Autenticacion.class.getResource("/iconos/logo.png")));

		setTitle("PaletSoft");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setSize(583, 320);
		setResizable(false);

		// Get the screen dimensions
		dim = Toolkit.getDefaultToolkit().getScreenSize();

		// Determine the window location
		w = getSize().width;
		h = getSize().height;
		x = (dim.width - w) / 2;
		y = (dim.height - h) / 2;

		// Center the window
		setLocation(x, y);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(268, 249, 10, -237);
		contentPane.add(separator);

		JLabel lblNombreDeUsuario = new JLabel("Nombre de Usuario");
		lblNombreDeUsuario.setBounds(296, 35, 122, 16);
		contentPane.add(lblNombreDeUsuario);

		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {

				PrincipalGUI principalGUI = new PrincipalGUI();
				principalGUI.setVisible(true);

				dispose();

			}
		});
		btnAceptar.setBounds(407, 235, 148, 34);
		contentPane.add(btnAceptar);

		JPanel panel = new JPanel();
		panel.setBounds(12, 2, 244, 267);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));

		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Autenticacion.class.getResource("/iconos/logo.png")));
		panel.add(lblNewLabel, BorderLayout.CENTER);

		JLabel lblClave = new JLabel("Contraseña");
		lblClave.setBounds(296, 130, 109, 16);
		contentPane.add(lblClave);

		txtNombreUsuario = new JTextField();
		txtNombreUsuario.setBounds(291, 63, 264, 26);
		contentPane.add(txtNombreUsuario);
		txtNombreUsuario.setColumns(10);

		txtClave = new JTextField();
		txtClave.setColumns(10);
		txtClave.setBounds(291, 151, 264, 26);
		contentPane.add(txtClave);
	}
}