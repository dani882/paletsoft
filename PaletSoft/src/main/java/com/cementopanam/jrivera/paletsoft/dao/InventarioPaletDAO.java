package com.cementopanam.jrivera.paletsoft.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.cementopanam.jrivera.paletsoft.entidad.InventarioPalet;
import com.cementopanam.jrivera.paletsoft.entidad.Palet;
import com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl;
import com.cementopanam.jrivera.paletsoft.util.HibernateUtil;

/**
 * Home object for domain model class InventarioPalet.
 * 
 * @see .InventarioPalet
 * @author Jesus Rivera
 */
public class InventarioPaletDAO implements PersistenceCRUDImpl<InventarioPalet, Integer> {

    private static final Logger LOGGER = LogManager.getLogger(InventarioPaletDAO.class);
    private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public void save(InventarioPalet inventarioPalet) {

        LOGGER.info("persisting InventarioPalet instance");

        try (Session session = sessionFactory.openSession();) {

            session.getTransaction().begin();
            session.save(inventarioPalet);
            session.getTransaction().commit();

        } catch (HibernateException hbe) {
            LOGGER.error(hbe.getMessage(), hbe);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Override
    public void update(InventarioPalet inventarioPalet) {
        // TODO Auto-generated method stub

    }

    @Override
    public InventarioPalet findById(Integer id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void delete(InventarioPalet inventarioPalet) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<InventarioPalet> findAll() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Get the PaletQuatity
     * 
     * @param name
     *            of the Palet
     * @return the quantity
     */
    public Integer getPaletQuantity(String name) {

        Integer inventarioActual = 0;
        Session session = null;
        DetachedCriteria criteria = null;
        try {
            session = sessionFactory.openSession();

            criteria = DetachedCriteria.forClass(InventarioPalet.class);
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            criteria.add(Restrictions.eq("nombreSindicato", name));

            inventarioActual = (Integer) criteria.getExecutableCriteria(session).list().get(0);
        }

        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            session.close();
        }

        return inventarioActual;
    }
    
    
    public List<InventarioPalet> getCurrentStock(Integer id) {

        List<InventarioPalet> inventarioPalets = null;
        Session session = null;
        try {

            session = sessionFactory.openSession();
            // Create CriteriaBuilder
            CriteriaBuilder builder = session.getCriteriaBuilder();
            // Create CriteriaQuery
            CriteriaQuery<InventarioPalet> criteriaQuery = builder.createQuery(InventarioPalet.class);
            // Specify criteria root
            Root<InventarioPalet> rootInventarioPalet = criteriaQuery.from(InventarioPalet.class);

            Join<InventarioPalet, Palet> stock = rootInventarioPalet.join("palet");

            inventarioPalets = session.createQuery(criteriaQuery).getResultList();
            // criteriaQuery.where(builder.equal(rootInventarioPalet.get("palet"),
            // id));

            // Join<InventarioPalet> stock = rootPalet.join("inventarioPalets");

            // select inventario_actual from palet_db.tblinventario_palet
            // inner join palet_db.tblpalet on tblpalet.codigo_palet =
            // tblinventario_palet.codigo_palet
            // where tblpalet.codigo_palet = 2;
        }

        catch (Exception e) {
        }

        return inventarioPalets;

    }

}
