package com.cementopanam.jrivera.paletsoft.gui.helper;

import javax.swing.table.AbstractTableModel;

public class CambioEstatusTable extends AbstractTableModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final int NUMERO_SAP = 0;
    private static final int CLIENTE = 1;
    private static final int ESTATUS = 2;

    String[] columns = { "Numero de Entrega SAP", "Cliente", "Estatus" };

    @Override
    public String getColumnName(int x) {
        return columns[x];
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public int getRowCount() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Object getValueAt(int arg0, int arg1) {

        Object result = null;
        
        
        return null;
    }
}
