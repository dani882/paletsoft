package com.cementopanam.jrivera.paletsoft.gui;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PaletaGUI extends JInternalFrame {
	private JTextField txtCantidad;
	private JTextField txtInventario;

	/**
	 * Create the frame.
	 */
	public PaletaGUI() {
		setTitle("Paleta");
		setBounds(100, 100, 416, 207);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblPaleta = new JLabel("Paleta");
		lblPaleta.setBounds(18, 34, 35, 16);
		panel.add(lblPaleta);
		
		JComboBox CBPaleta = new JComboBox();
		CBPaleta.setBounds(85, 29, 242, 26);
		panel.add(CBPaleta);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {
				/*
				select inventario_actual into salida from tblinventario_palet
			    inner join tblpalet  on tblpalet.codigo_palet = tblinventario_palet.codigo_palet
			    where tblpalet.codigo_palet = valor_entrada;
			    */
				//TODO Hibernate de Listado de busqueda
				
			}
		});
		btnNewButton.setBounds(339, 29, 59, 28);
		panel.add(btnNewButton);
		
		JLabel lblCantidad = new JLabel("Cantidad");
		lblCantidad.setBounds(18, 86, 55, 16);
		panel.add(lblCantidad);
		
		txtCantidad = new JTextField();
		txtCantidad.setBounds(85, 80, 122, 28);
		panel.add(txtCantidad);
		txtCantidad.setColumns(10);
		
		JLabel lblInventario = new JLabel("Inventario");
		lblInventario.setBounds(219, 86, 55, 16);
		panel.add(lblInventario);
		
		txtInventario = new JTextField();
		txtInventario.setBounds(276, 80, 122, 28);
		panel.add(txtInventario);
		txtInventario.setColumns(10);
		
		JPanel panelBotones = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panelBotones.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		getContentPane().add(panelBotones, BorderLayout.SOUTH);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {
				
			}
		});
		panelBotones.add(btnAgregar);
		
		JButton btnCancelar = new JButton("Cancelar");
		panelBotones.add(btnCancelar);

	}
}
