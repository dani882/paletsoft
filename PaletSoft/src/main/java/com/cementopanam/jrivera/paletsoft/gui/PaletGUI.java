package com.cementopanam.jrivera.paletsoft.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cementopanam.jrivera.paletsoft.dao.InventarioPaletDAO;
import com.cementopanam.jrivera.paletsoft.dao.PaletDAO;
import com.cementopanam.jrivera.paletsoft.entidad.InventarioPalet;
import com.cementopanam.jrivera.paletsoft.entidad.Palet;

/**
 * Interfaz de UsuarioDAO para registrar y actualizar Paletas
 * 
 * @author jrivera
 *
 */
public class PaletGUI extends JInternalFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger(PaletGUI.class);
    private JTextField txtInventarioActual;
    private JTextField txtCantidad;
    private JTextField txtCodigoPalet;
    private JTextField txtDescripcionPalet;
    private JTextField txtCargaInicial;
    private JComboBox<Object> cBPalet;

    /**
     * Create the frame.
     */
    public PaletGUI() {
        getContentPane().setFont(new Font("Verdana", Font.PLAIN, 12));

        JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.TOP);
        tabbedPane.setFont(new Font("Verdana", Font.PLAIN, 12));
        getContentPane().add(tabbedPane, BorderLayout.CENTER);

        JPanel panelRegistro = new JPanel();
        panelRegistro.setBackground(Color.WHITE);
        tabbedPane.addTab("Registro de Palet", null, panelRegistro, null);
        panelRegistro.setLayout(null);

        JLabel lblCodigoDePalet = new JLabel("Codigo de Palet");
        lblCodigoDePalet.setFont(new Font("Verdana", Font.PLAIN, 12));
        lblCodigoDePalet.setBounds(10, 21, 108, 14);
        panelRegistro.add(lblCodigoDePalet);

        JLabel lblDescripcionDePalet = new JLabel("Descripcion de Palet");
        lblDescripcionDePalet.setFont(new Font("Verdana", Font.PLAIN, 12));
        lblDescripcionDePalet.setBounds(10, 63, 163, 14);
        panelRegistro.add(lblDescripcionDePalet);

        JLabel lblCargaInicial = new JLabel("Carga Inicial");
        lblCargaInicial.setFont(new Font("Verdana", Font.PLAIN, 12));
        lblCargaInicial.setBounds(10, 105, 108, 14);
        panelRegistro.add(lblCargaInicial);

        txtCodigoPalet = new JTextField();
        txtCodigoPalet.setFont(new Font("Verdana", Font.PLAIN, 12));
        txtCodigoPalet.setBounds(185, 19, 101, 27);
        panelRegistro.add(txtCodigoPalet);
        txtCodigoPalet.setColumns(10);

        JButton btnGuardar_1 = new JButton("Guardar");
        btnGuardar_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                registrarPalet();
            }
        });
        btnGuardar_1.setFont(new Font("Verdana", Font.PLAIN, 12));
        btnGuardar_1.setBounds(269, 156, 89, 23);
        panelRegistro.add(btnGuardar_1);

        JButton btnSalir_1 = new JButton("Salir");
        btnSalir_1.setFont(new Font("Verdana", Font.PLAIN, 12));
        btnSalir_1.setBounds(368, 156, 89, 23);
        panelRegistro.add(btnSalir_1);

        txtDescripcionPalet = new JTextField();
        txtDescripcionPalet.setFont(new Font("Verdana", Font.PLAIN, 12));
        txtDescripcionPalet.setBounds(185, 61, 272, 27);
        panelRegistro.add(txtDescripcionPalet);
        txtDescripcionPalet.setColumns(10);

        txtCargaInicial = new JTextField();
        txtCargaInicial.setFont(new Font("Verdana", Font.PLAIN, 12));
        txtCargaInicial.setBounds(185, 103, 101, 27);
        panelRegistro.add(txtCargaInicial);
        txtCargaInicial.setColumns(10);

        JPanel panelActualizacion = new JPanel();
        panelActualizacion.setBackground(Color.WHITE);
        tabbedPane.addTab("Actualizacion de Palet", null, panelActualizacion, null);
        panelActualizacion.setLayout(null);

        JLabel lblPalet = new JLabel("Palet");
        lblPalet.setFont(new Font("Verdana", Font.PLAIN, 12));
        lblPalet.setBounds(10, 17, 46, 14);
        panelActualizacion.add(lblPalet);

        JLabel lblInventarioActual = new JLabel("Inventario Actual");
        lblInventarioActual.setFont(new Font("Verdana", Font.PLAIN, 12));
        lblInventarioActual.setBounds(10, 62, 121, 22);
        panelActualizacion.add(lblInventarioActual);

        JLabel lblCantidadAIngresar = new JLabel("Cantidad a Ingresar");
        lblCantidadAIngresar.setFont(new Font("Verdana", Font.PLAIN, 12));
        lblCantidadAIngresar.setBounds(10, 96, 137, 22);
        panelActualizacion.add(lblCantidadAIngresar);

        cBPalet = new JComboBox<Object>();

        cBPalet.setModel(new DefaultComboBoxModel<Object>());
        PaletDAO paletDAO = new PaletDAO();

        try {

            for (Palet palet : paletDAO.findAll()) {

                cBPalet.addItem(palet.getDescripcion());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        cBPalet.setSelectedIndex(-1);
        cBPalet.setFont(new Font("Verdana", Font.PLAIN, 11));
        cBPalet.setBounds(157, 11, 203, 27);
        panelActualizacion.add(cBPalet);

        JButton btnVerificar = new JButton("Verificar");
        btnVerificar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                verificarInventarioPalet();
            }
        });
        btnVerificar.setFont(new Font("Verdana", Font.PLAIN, 12));
        btnVerificar.setBounds(368, 11, 89, 27);
        panelActualizacion.add(btnVerificar);

        JButton btnGuardar = new JButton("Guardar");
        btnGuardar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                actualizarInventarioPalet();
            }
        });
        btnGuardar.setFont(new Font("Verdana", Font.PLAIN, 12));
        btnGuardar.setBounds(271, 156, 89, 23);
        panelActualizacion.add(btnGuardar);

        JButton btnSalir = new JButton("Salir");
        btnSalir.setFont(new Font("Verdana", Font.PLAIN, 12));
        btnSalir.setBounds(368, 156, 89, 23);
        panelActualizacion.add(btnSalir);

        txtInventarioActual = new JTextField();
        txtInventarioActual.setFont(new Font("Verdana", Font.PLAIN, 12));
        txtInventarioActual.setBounds(157, 57, 116, 27);
        panelActualizacion.add(txtInventarioActual);
        txtInventarioActual.setColumns(10);

        txtCantidad = new JTextField();
        txtCantidad.setFont(new Font("Verdana", Font.PLAIN, 12));
        txtCantidad.setColumns(10);
        txtCantidad.setBounds(157, 95, 116, 27);
        panelActualizacion.add(txtCantidad);
        setTitle("Palet");
        setBounds(100, 100, 488, 250);

    }

    protected void actualizarInventarioPalet() {
        // TODO Buscar codigo de descripcion Palet

    }

    protected void registrarPalet() {
        // TODO Auto-generated method stub

    }

    protected void verificarInventarioPalet() {
        // TODO Auto-generated method stub

        PaletDAO paletDAO = new PaletDAO();
        String paleta = String.valueOf(cBPalet.getSelectedItem());

        int cantidadPalet = paletDAO.findPaletID(paleta);
        txtInventarioActual.setText(String.valueOf(cantidadPalet));

        InventarioPaletDAO inventarioPalet = new InventarioPaletDAO();
        List<InventarioPalet> inventario = inventarioPalet.getCurrentStock(cantidadPalet);
        
        for (InventarioPalet inventarioPalet2 : inventario) {
            System.out.println("La cantidad es: " + inventarioPalet2.getInventarioActual());
        }
        
    }
}