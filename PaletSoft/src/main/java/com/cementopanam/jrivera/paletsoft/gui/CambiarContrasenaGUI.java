package com.cementopanam.jrivera.paletsoft.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class CambiarContrasenaGUI extends JInternalFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JTextField txtClaveAnterior;
    private JTextField txtClaveNueva;
    private JTextField txtConfirmacionClave;

    /**
     * Create the dialog.
     */
    public CambiarContrasenaGUI() {
        setClosable(true);
        setIconifiable(true);
        setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        setTitle("Cambiar Contraseña");
        setBounds(100, 100, 400, 213);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new BorderLayout(0, 0));
        {
            JPanel panel = new JPanel();
            panel.setBorder(new LineBorder(Color.LIGHT_GRAY));
            contentPanel.add(panel, BorderLayout.CENTER);
            panel.setLayout(null);

            JLabel lblContraseaAnterior = new JLabel("Contraseña anterior");
            lblContraseaAnterior.setFont(new Font("Verdana", Font.PLAIN, 12));
            lblContraseaAnterior.setBounds(10, 20, 137, 22);
            panel.add(lblContraseaAnterior);

            txtClaveAnterior = new JTextField();
            txtClaveAnterior.setFont(new Font("Verdana", Font.PLAIN, 12));
            txtClaveAnterior.setBounds(199, 18, 165, 24);
            panel.add(txtClaveAnterior);
            txtClaveAnterior.setColumns(10);

            JLabel lblClaveNueva = new JLabel("Contraseña nueva");
            lblClaveNueva.setFont(new Font("Verdana", Font.PLAIN, 12));
            lblClaveNueva.setBounds(10, 53, 137, 22);
            panel.add(lblClaveNueva);

            txtClaveNueva = new JTextField();
            txtClaveNueva.setFont(new Font("Verdana", Font.PLAIN, 12));
            txtClaveNueva.setColumns(10);
            txtClaveNueva.setBounds(199, 51, 165, 24);
            panel.add(txtClaveNueva);

            JLabel lblConfirmacionClave = new JLabel("Confirmacion de Contraseña");
            lblConfirmacionClave.setFont(new Font("Verdana", Font.PLAIN, 12));
            lblConfirmacionClave.setBounds(10, 86, 179, 22);
            panel.add(lblConfirmacionClave);

            txtConfirmacionClave = new JTextField();
            txtConfirmacionClave.setFont(new Font("Verdana", Font.PLAIN, 12));
            txtConfirmacionClave.setColumns(10);
            txtConfirmacionClave.setBounds(199, 84, 165, 24);
            panel.add(txtConfirmacionClave);
        }
        {
            JPanel panel = new JPanel();
            contentPanel.add(panel, BorderLayout.NORTH);
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("Cambiar");
                okButton.setFont(new Font("Verdana", Font.PLAIN, 12));
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
            {
                JButton cancelButton = new JButton("Cancelar");
                cancelButton.setFont(new Font("Verdana", Font.PLAIN, 12));
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
            }
        }
    }
}