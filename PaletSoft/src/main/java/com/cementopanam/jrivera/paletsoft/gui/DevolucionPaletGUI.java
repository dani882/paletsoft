package com.cementopanam.jrivera.paletsoft.gui;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import javax.swing.UIManager;

public class DevolucionPaletGUI extends JInternalFrame {
	
	private JTextField txtNumeroEntrega;
	private JTextField txtCliente;
	private JTextField txtConductor;
	private JTable table;
	private JTable table_1;


	/**
	 * Create the frame.
	 */
	public DevolucionPaletGUI() {
		setTitle("Devolver PaletGUI");
		setBounds(100, 100, 797, 576);
		getContentPane().setLayout(null);
		
		JPanel panelFiltros = new JPanel();
		panelFiltros.setBounds(10, 11, 653, 117);
		getContentPane().add(panelFiltros);
		panelFiltros.setLayout(null);
		
		JRadioButton rdbtnFiltrarPorNumero = new JRadioButton("Filtrar por Numero Entrega");
		rdbtnFiltrarPorNumero.setFont(new Font("SansSerif", Font.PLAIN, 12));
		rdbtnFiltrarPorNumero.setBounds(6, 7, 186, 23);
		panelFiltros.add(rdbtnFiltrarPorNumero);
		
		JRadioButton rdbtnFiltrarPorCliente = new JRadioButton("Filtrar por ClienteGUI");
		rdbtnFiltrarPorCliente.setFont(new Font("SansSerif", Font.PLAIN, 12));
		rdbtnFiltrarPorCliente.setBounds(6, 44, 186, 23);
		panelFiltros.add(rdbtnFiltrarPorCliente);
		
		JRadioButton rdbtnFiltrarPorConductor = new JRadioButton("Filtrar por Conductor");
		rdbtnFiltrarPorConductor.setFont(new Font("SansSerif", Font.PLAIN, 12));
		rdbtnFiltrarPorConductor.setBounds(6, 83, 186, 23);
		panelFiltros.add(rdbtnFiltrarPorConductor);
		
		JLabel lblIngresarNumeroDe = new JLabel("Ingresar Numero de Entrega");
		lblIngresarNumeroDe.setFont(new Font("SansSerif", Font.PLAIN, 12));
		lblIngresarNumeroDe.setBounds(235, 11, 157, 19);
		panelFiltros.add(lblIngresarNumeroDe);
		
		JLabel lblEscogerCliente = new JLabel("Escoger ClienteGUI");
		lblEscogerCliente.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEscogerCliente.setFont(new Font("SansSerif", Font.PLAIN, 12));
		lblEscogerCliente.setBounds(235, 46, 157, 19);
		panelFiltros.add(lblEscogerCliente);
		
		JLabel lblEscogerConductor = new JLabel("Escoger Conductor");
		lblEscogerConductor.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEscogerConductor.setFont(new Font("SansSerif", Font.PLAIN, 12));
		lblEscogerConductor.setBounds(235, 85, 157, 19);
		panelFiltros.add(lblEscogerConductor);
		
		txtNumeroEntrega = new JTextField();
		txtNumeroEntrega.setBounds(402, 9, 142, 28);
		panelFiltros.add(txtNumeroEntrega);
		txtNumeroEntrega.setColumns(10);
		
		txtCliente = new JTextField();
		txtCliente.setColumns(10);
		txtCliente.setBounds(402, 42, 142, 28);
		panelFiltros.add(txtCliente);
		
		txtConductor = new JTextField();
		txtConductor.setColumns(10);
		txtConductor.setBounds(402, 78, 142, 28);
		panelFiltros.add(txtConductor);
		
		JButton button = new JButton("New button");
		button.setBounds(554, 11, 89, 96);
		panelFiltros.add(button);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.setBounds(682, 105, 89, 23);
		getContentPane().add(btnNewButton);
		
		JPanel panelPendienteRecibir = new JPanel();
		panelPendienteRecibir.setBorder(new TitledBorder(null, "Pendiente de Recibir", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelPendienteRecibir.setBounds(10, 139, 761, 197);
		getContentPane().add(panelPendienteRecibir);
		panelPendienteRecibir.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panelPendienteRecibir.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JPanel panelRecibido = new JPanel();
		panelRecibido.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "PaletGUI Recibido", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelRecibido.setBounds(10, 345, 761, 152);
		getContentPane().add(panelRecibido);
		panelRecibido.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		panelRecibido.add(scrollPane_1);
		
		table_1 = new JTable();
		scrollPane_1.setViewportView(table_1);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnSalir.setBounds(682, 512, 89, 23);
		getContentPane().add(btnSalir);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnGuardar.setBounds(583, 512, 89, 23);
		getContentPane().add(btnGuardar);

	}
}
