package com.cementopanam.jrivera.paletsoft.dao;
// default package
// Generated Sep 13, 2016 8:29:28 PM by Hibernate Tools 5.1.0.Beta1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class AuditoriaInventario.
 * @see .TblauditoriaInventario
 * @author Hibernate Tools
 */
public class AuditoriaInventarioDAO {

	private static final Logger log = LogManager.getLogger(AuditoriaInventarioDAO.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(AuditoriaInventarioDAO transientInstance) {
		log.debug("persisting AuditoriaInventario instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(AuditoriaInventarioDAO instance) {
		log.debug("attaching dirty AuditoriaInventario instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(AuditoriaInventarioDAO instance) {
		log.debug("attaching clean AuditoriaInventario instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(AuditoriaInventarioDAO persistentInstance) {
		log.debug("deleting AuditoriaInventario instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public AuditoriaInventarioDAO merge(AuditoriaInventarioDAO detachedInstance) {
		log.debug("merging AuditoriaInventario instance");
		try {
			AuditoriaInventarioDAO result = (AuditoriaInventarioDAO) sessionFactory.getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public AuditoriaInventarioDAO findById(java.lang.Integer id) {
		log.debug("getting AuditoriaInventario instance with id: " + id);
		try {
			AuditoriaInventarioDAO instance = (AuditoriaInventarioDAO) sessionFactory.getCurrentSession()
					.get("AuditoriaInventario", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(AuditoriaInventarioDAO instance) {
		log.debug("finding AuditoriaInventario instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("AuditoriaInventario")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
