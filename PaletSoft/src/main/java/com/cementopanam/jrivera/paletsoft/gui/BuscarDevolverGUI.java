package com.cementopanam.jrivera.paletsoft.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

public class BuscarDevolverGUI extends JInternalFrame {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JTextField txtNombreCliente;
    private JTable tableCliente;
    private JTextField txtNombreConductor;
    private JTable tableConductor;

    /**
     * Create the frame.
     */
    public BuscarDevolverGUI() {
        getContentPane().setFont(new Font("Verdana", Font.PLAIN, 12));
        getContentPane().setLayout(new BorderLayout(0, 0));

        JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.TOP);
        getContentPane().add(tabbedPane, BorderLayout.CENTER);

        JPanel panelCliente = new JPanel();
        panelCliente.setBackground(Color.WHITE);
        tabbedPane.addTab("Cliente", null, panelCliente, null);
        panelCliente.setLayout(null);

        JPanel panelFiltroCliente = new JPanel();
        panelFiltroCliente.setBackground(Color.WHITE);
        panelFiltroCliente.setBounds(10, 11, 363, 55);
        panelCliente.add(panelFiltroCliente);
        panelFiltroCliente
                .setBorder(new TitledBorder(null, "Filtro", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        panelFiltroCliente.setLayout(null);

        JLabel lblNombreCliente = new JLabel("Introducir Nombre");
        lblNombreCliente.setBounds(10, 24, 114, 16);
        lblNombreCliente.setFont(new Font("Verdana", Font.PLAIN, 12));
        panelFiltroCliente.add(lblNombreCliente);

        txtNombreCliente = new JTextField();
        txtNombreCliente.setBounds(134, 21, 219, 23);
        txtNombreCliente.setFont(new Font("Verdana", Font.PLAIN, 12));
        txtNombreCliente.setColumns(10);
        panelFiltroCliente.add(txtNombreCliente);

        JPanel panelTableCliente = new JPanel();
        panelTableCliente.setBackground(Color.GRAY);
        panelTableCliente.setBounds(10, 77, 459, 154);
        panelCliente.add(panelTableCliente);
        panelTableCliente.setLayout(null);

        JScrollPane scrollPaneTableCliente = new JScrollPane();
        scrollPaneTableCliente.setBounds(0, 0, 459, 154);
        panelTableCliente.add(scrollPaneTableCliente);

        tableCliente = new JTable();
        tableCliente.setFont(new Font("Verdana", Font.PLAIN, 12));
        scrollPaneTableCliente.setViewportView(tableCliente);

        JPanel panelConductor = new JPanel();
        panelConductor.setLayout(null);
        panelConductor.setBackground(Color.WHITE);
        tabbedPane.addTab("Conductor", null, panelConductor, null);

        JPanel panelFiltroConductor = new JPanel();
        panelFiltroConductor.setLayout(null);
        panelFiltroConductor
                .setBorder(new TitledBorder(null, "Filtro", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        panelFiltroConductor.setBackground(Color.WHITE);
        panelFiltroConductor.setBounds(10, 11, 363, 55);
        panelConductor.add(panelFiltroConductor);

        JLabel lblNombreConductor = new JLabel("Introducir Nombre");
        lblNombreConductor.setFont(new Font("Verdana", Font.PLAIN, 12));
        lblNombreConductor.setBounds(10, 24, 114, 16);
        panelFiltroConductor.add(lblNombreConductor);

        txtNombreConductor = new JTextField();
        txtNombreConductor.setFont(new Font("Verdana", Font.PLAIN, 12));
        txtNombreConductor.setColumns(10);
        txtNombreConductor.setBounds(134, 21, 219, 23);
        panelFiltroConductor.add(txtNombreConductor);

        JPanel panelTableConductor = new JPanel();
        panelTableConductor.setBackground(Color.GRAY);
        panelTableConductor.setBounds(10, 77, 459, 154);
        panelConductor.add(panelTableConductor);
        panelTableConductor.setLayout(null);

        JScrollPane scrollPaneTableConductor = new JScrollPane();
        scrollPaneTableConductor.setBounds(0, 0, 459, 154);
        panelTableConductor.add(scrollPaneTableConductor);

        tableConductor = new JTable();
        scrollPaneTableConductor.setViewportView(tableConductor);
        setIconifiable(true);
        setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        setClosable(true);
        setTitle("Buscar");
        setBounds(100, 100, 500, 300);

    }
}