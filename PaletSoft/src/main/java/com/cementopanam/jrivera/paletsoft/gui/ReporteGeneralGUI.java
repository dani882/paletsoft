package com.cementopanam.jrivera.paletsoft.gui;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import com.toedter.calendar.JDateChooser;

public class ReporteGeneralGUI extends JInternalFrame {


	/**
	 * Create the frame.
	 */
	public ReporteGeneralGUI() {
		setTitle("Reporte General de Palet");
		setBounds(100, 100, 664, 400);
		
		JPanel panelFiltro = new JPanel();
		getContentPane().add(panelFiltro, BorderLayout.NORTH);
		
		JLabel lblDesde = new JLabel("Desde");
		lblDesde.setFont(new Font("SansSerif", Font.PLAIN, 12));
		panelFiltro.add(lblDesde);
		
		JLabel lblHasta = new JLabel("Hasta");
		lblHasta.setFont(new Font("SansSerif", Font.PLAIN, 12));
		panelFiltro.add(lblHasta);
		
		JDateChooser dCDesde = new JDateChooser();
		panelFiltro.add(dCDesde);
		
		JDateChooser dCHasta = new JDateChooser();
		panelFiltro.add(dCHasta);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setFont(new Font("SansSerif", Font.PLAIN, 12));
		panelFiltro.add(btnBuscar);
		
		JPanel panelResultado = new JPanel();
		getContentPane().add(panelResultado, BorderLayout.CENTER);

	}

}
