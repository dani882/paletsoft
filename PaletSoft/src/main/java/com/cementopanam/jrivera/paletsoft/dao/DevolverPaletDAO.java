package com.cementopanam.jrivera.paletsoft.dao;

// Generated Sep 13, 2016 8:29:28 PM by Hibernate Tools 5.1.0.Beta1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class DevolverPalet.
 * 
 * @see .TbldevolverPalet
 * @author Hibernate Tools
 */
public class DevolverPaletDAO {

	private static final Logger log = LogManager.getLogger(DevolverPaletDAO.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(DevolverPaletDAO transientInstance) {
		log.debug("persisting DevolverPalet instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(DevolverPaletDAO instance) {
		log.debug("attaching dirty DevolverPalet instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(DevolverPaletDAO instance) {
		log.debug("attaching clean DevolverPalet instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(DevolverPaletDAO persistentInstance) {
		log.debug("deleting DevolverPalet instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public DevolverPaletDAO merge(DevolverPaletDAO detachedInstance) {
		log.debug("merging DevolverPalet instance");
		try {
			DevolverPaletDAO result = (DevolverPaletDAO) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public DevolverPaletDAO findById(java.lang.Integer id) {
		log.debug("getting DevolverPalet instance with id: " + id);
		try {
			DevolverPaletDAO instance = (DevolverPaletDAO) sessionFactory.getCurrentSession().get("DevolverPalet",
					id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(DevolverPaletDAO instance) {
		log.debug("finding DevolverPalet instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("DevolverPalet")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
