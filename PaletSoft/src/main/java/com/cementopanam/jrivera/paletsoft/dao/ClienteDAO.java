package com.cementopanam.jrivera.paletsoft.dao;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;

import com.cementopanam.jrivera.paletsoft.entidad.Cliente;
import com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl;
import com.cementopanam.jrivera.paletsoft.util.HibernateUtil;

/**
 * Home object for domain model class ClienteGUI.
 * 
 * @see .Cliente
 * @author Jesus Rivera
 */
public class ClienteDAO implements PersistenceCRUDImpl<Cliente, Integer> {

    private static final Logger LOGGER = LogManager.getLogger(ClienteDAO.class);
    private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    /*
     * Save the object in the database
     * 
     * @see
     * com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl#save(java.lang.
     * Object)
     */
    @Override
    public void save(Cliente cliente) {

        LOGGER.info("persisting Cliente instance");

        try (Session session = sessionFactory.openSession();) {

            session.getTransaction().begin();
            session.save(cliente);
            session.getTransaction().commit();

        } catch (HibernateException hbe) {
            LOGGER.error(hbe.getMessage(), hbe);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Override
    public void update(Cliente cliente) {

        LOGGER.info("updating Cliente instance");

        try (Session session = sessionFactory.openSession();) {

            session.getTransaction().begin();
            session.update(cliente);
            session.getTransaction().commit();

        } catch (HibernateException hbe) {
            LOGGER.error(hbe.getMessage(), hbe);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Override
    public Cliente findById(Integer id) {

        Session session = sessionFactory.getCurrentSession();
        Cliente cliente = session.get(Cliente.class, id);
        session.close();

        return cliente;
    }

    @Override
    public void delete(Cliente cliente) {

        LOGGER.info("deleting Cliente instance");

        try (Session session = sessionFactory.openSession();) {

            Cliente ch = findById(cliente.getCodigoCliente());
            // TODO Modificar para borrar mediante otros parametros ya que solo
            // borra por codigo
            session.getTransaction().begin();
            session.delete(ch);
            session.getTransaction().commit();
        }

    }

    /*
     * Search for all Clientes
     * 
     * @see com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl#findAll()
     * 
     * @return List of Clientes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Cliente> findAll() {

        List<Cliente> clientes = null;
        Session session = null;
        DetachedCriteria criteria = null;

        try {
            session = sessionFactory.openSession();

            criteria = DetachedCriteria.forClass(Cliente.class);
            clientes = criteria.getExecutableCriteria(session).list();

        }

        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            session.close();
        }

        return clientes;
    }
}