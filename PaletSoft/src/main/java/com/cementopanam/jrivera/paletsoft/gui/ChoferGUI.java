package com.cementopanam.jrivera.paletsoft.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cementopanam.jrivera.paletsoft.dao.ChoferDAO;
import com.cementopanam.jrivera.paletsoft.dao.SindicatoDAO;
import com.cementopanam.jrivera.paletsoft.entidad.Chofer;
import com.cementopanam.jrivera.paletsoft.entidad.Sindicato;
import com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl;

/**
 * Interfaz de ChoferDAO para el Registro de choferes y Sindicatos
 * 
 * @author Jesus Rivera
 *
 */
public class ChoferGUI extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6221420999911939982L;
	private static final Logger LOGGER = LogManager.getLogger(ChoferGUI.class);
	private final JPanel contentPanel = new JPanel();
	private JTextField txtCodChofer;
	private JTextField txtNroCedula;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtCodigoSindicato;
	private JTextField txtNombreSindicato;
	private JCheckBox chckbxActivo;
	private JComboBox<Object> cbSindicato;
	private JCheckBox chEstatus;

	private int codigoSindicato = 0;

	/**
	 * Create the dialog.
	 */
	public ChoferGUI() {

		setIconifiable(true);
		setFont(new Font("Verdana", Font.PLAIN, 12));
		setResizable(false);
		setTitle("Chofer");
		setBounds(100, 100, 500, 321);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JTabbedPane tabRegistro = new JTabbedPane(SwingConstants.TOP);
			contentPanel.add(tabRegistro, BorderLayout.CENTER);
			{
				JPanel panelChofer = new JPanel();
				panelChofer.setBackground(Color.WHITE);
				panelChofer.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
				tabRegistro.addTab("Registro de Chofer", null, panelChofer, null);
				panelChofer.setLayout(null);

				JLabel lblCodigoDeChofer = new JLabel("Codigo de Chofer");
				lblCodigoDeChofer.setFont(new Font("Verdana", Font.PLAIN, 12));
				lblCodigoDeChofer.setBounds(10, 13, 129, 22);
				panelChofer.add(lblCodigoDeChofer);

				JLabel lblNroDeCedula = new JLabel("Nro de Cedula");
				lblNroDeCedula.setFont(new Font("Verdana", Font.PLAIN, 12));
				lblNroDeCedula.setBounds(10, 46, 129, 22);
				panelChofer.add(lblNroDeCedula);

				JLabel lblNombre = new JLabel("Nombre");
				lblNombre.setFont(new Font("Verdana", Font.PLAIN, 12));
				lblNombre.setBounds(10, 82, 129, 22);
				panelChofer.add(lblNombre);

				JLabel lblApellido = new JLabel("Apellido");
				lblApellido.setFont(new Font("Verdana", Font.PLAIN, 12));
				lblApellido.setBounds(10, 118, 129, 22);
				panelChofer.add(lblApellido);

				JLabel lblSindicato = new JLabel("Sindicato");
				lblSindicato.setFont(new Font("Verdana", Font.PLAIN, 12));
				lblSindicato.setBounds(10, 156, 129, 22);
				panelChofer.add(lblSindicato);

				JLabel lblEstatus = new JLabel("Estatus");
				lblEstatus.setFont(new Font("Verdana", Font.PLAIN, 12));
				lblEstatus.setBounds(10, 192, 129, 21);
				panelChofer.add(lblEstatus);

				txtCodChofer = new JTextField();
				txtCodChofer.setEnabled(false);
				txtCodChofer.setEditable(false);
				txtCodChofer.setFont(new Font("Verdana", Font.PLAIN, 12));
				txtCodChofer.setBounds(134, 13, 112, 24);
				panelChofer.add(txtCodChofer);
				txtCodChofer.setColumns(10);

				txtNroCedula = new JTextField();
				txtNroCedula.setFont(new Font("Verdana", Font.PLAIN, 12));
				txtNroCedula.setColumns(10);
				txtNroCedula.setBounds(134, 46, 224, 24);
				panelChofer.add(txtNroCedula);

				txtNombre = new JTextField();
				txtNombre.setFont(new Font("Verdana", Font.PLAIN, 12));
				txtNombre.setColumns(10);
				txtNombre.setBounds(134, 82, 224, 24);
				panelChofer.add(txtNombre);

				txtApellido = new JTextField();
				txtApellido.setFont(new Font("Verdana", Font.PLAIN, 12));
				txtApellido.setColumns(10);
				txtApellido.setBounds(134, 118, 224, 24);
				panelChofer.add(txtApellido);

				chEstatus = new JCheckBox("Activo");
				chEstatus.setFont(new Font("Verdana", Font.PLAIN, 12));
				chEstatus.setBackground(Color.WHITE);
				chEstatus.setBounds(130, 191, 97, 24);
				panelChofer.add(chEstatus);

				cbSindicato = new JComboBox<Object>();
				cbSindicato.addActionListener(new ActionListener() {
					@Override
                    public void actionPerformed(ActionEvent e) {

						SindicatoDAO sindicatoDAO = new SindicatoDAO();
						codigoSindicato = sindicatoDAO.findByName(String.valueOf(cbSindicato.getSelectedItem()));

					}
				});

				cbSindicato.setModel(new DefaultComboBoxModel<Object>());
				ChoferDAO chofer = new ChoferDAO();

				try {

					for (Sindicato sindicato : chofer.findAllSindicatos()) {

						cbSindicato.addItem(sindicato.getNombreSindicato());
					}
				} catch (Exception e) {
					LOGGER.error(e.getMessage(), e);
				}

				cbSindicato.setFont(new Font("Verdana", Font.PLAIN, 12));
				cbSindicato.setBounds(134, 156, 224, 24);
				panelChofer.add(cbSindicato);
				{
					JButton okButton = new JButton("Guardar");
					okButton.addActionListener(new ActionListener() {
						@Override
                        public void actionPerformed(ActionEvent e) {

							saveChofer();

						}
					});
					okButton.setFont(new Font("Verdana", Font.PLAIN, 12));
					okButton.setBounds(293, 220, 86, 23);
					panelChofer.add(okButton);
					okButton.setActionCommand("OK");
					getRootPane().setDefaultButton(okButton);
				}
				{
					JButton cancelButton = new JButton("Salir");
					cancelButton.addActionListener(new ActionListener() {
						@Override
                        public void actionPerformed(ActionEvent e) {
							dispose();
						}
					});
					cancelButton.setFont(new Font("Verdana", Font.PLAIN, 12));
					cancelButton.setBounds(383, 220, 86, 23);
					panelChofer.add(cancelButton);
					cancelButton.setActionCommand("Cancel");
				}
			}
			{
				JPanel panelSindicado = new JPanel();
				panelSindicado.setBackground(Color.WHITE);
				tabRegistro.addTab("Registro de Sindicato", null, panelSindicado, null);
				panelSindicado.setLayout(null);

				JButton btnGuardarSindicado = new JButton("Guardar");
				btnGuardarSindicado.addActionListener(new ActionListener() {
					@Override
                    public void actionPerformed(ActionEvent e) {

						saveSindicato();

					}
				});
				btnGuardarSindicado.setBounds(292, 220, 86, 23);
				btnGuardarSindicado.setFont(new Font("Verdana", Font.PLAIN, 12));
				btnGuardarSindicado.setActionCommand("OK");
				panelSindicado.add(btnGuardarSindicado);

				JButton btnSalirSindicato = new JButton("Salir");
				btnSalirSindicato.addActionListener(new ActionListener() {
					@Override
                    public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				btnSalirSindicato.setBounds(383, 220, 86, 23);
				btnSalirSindicato.setFont(new Font("Verdana", Font.PLAIN, 12));
				btnSalirSindicato.setActionCommand("Cancel");
				panelSindicado.add(btnSalirSindicato);

				JLabel lblCodigoDeSindicato = new JLabel("Codigo de Sindicato");
				lblCodigoDeSindicato.setFont(new Font("Verdana", Font.PLAIN, 12));
				lblCodigoDeSindicato.setBounds(10, 24, 135, 26);
				panelSindicado.add(lblCodigoDeSindicato);

				JLabel lblNombreDelSindicato = new JLabel("Nombre del Sindicato");
				lblNombreDelSindicato.setFont(new Font("Verdana", Font.PLAIN, 12));
				lblNombreDelSindicato.setBounds(10, 61, 135, 25);
				panelSindicado.add(lblNombreDelSindicato);

				txtCodigoSindicato = new JTextField();
				txtCodigoSindicato.setEnabled(false);
				txtCodigoSindicato.setEditable(false);
				txtCodigoSindicato.setBounds(166, 22, 86, 28);
				panelSindicado.add(txtCodigoSindicato);
				txtCodigoSindicato.setColumns(10);

				txtNombreSindicato = new JTextField();
				txtNombreSindicato.setColumns(10);
				txtNombreSindicato.setBounds(166, 59, 202, 27);
				panelSindicado.add(txtNombreSindicato);

				JLabel lblEstatus_1 = new JLabel("Estatus");
				lblEstatus_1.setFont(new Font("Verdana", Font.PLAIN, 12));
				lblEstatus_1.setBounds(10, 106, 72, 14);
				panelSindicado.add(lblEstatus_1);

				chckbxActivo = new JCheckBox("Activo");
				chckbxActivo.setBackground(Color.WHITE);
				chckbxActivo.setFont(new Font("Verdana", Font.PLAIN, 12));
				chckbxActivo.setBounds(166, 103, 97, 23);
				panelSindicado.add(chckbxActivo);
			}
		}
	}

	/**
	 * Save a new Sindicato
	 */
	private void saveSindicato() {

		String nombreSindicato = txtNombreSindicato.getText();

		if (nombreSindicato.trim().equals("")) {
			JOptionPane.showMessageDialog(null, "Debe escribir el nombre del Sindicato");
			return;
		}

		String estatus = "Inactivo";

		if (chckbxActivo.isSelected()) {
			estatus = "Activo";
		}

		PersistenceCRUDImpl<Sindicato, Integer> sindicatoDAO = new SindicatoDAO();
		sindicatoDAO.save(new Sindicato(nombreSindicato, estatus));

		clean();
	}

	private void saveChofer() {

		String cedula = txtNroCedula.getText();
		String nombreChofer = txtNombre.getText();
		String apellidoChofer = txtApellido.getText();
		// String sindicato = String.valueOf(cbSindicato.getSelectedItem());

		String estatusChofer = "Inactivo";

		if (chEstatus.isSelected()) {
			estatusChofer = "Activo";
		}

		// Check for empty fields
		if (cedula.trim().equals("") || nombreChofer.trim().equals("") || apellidoChofer.trim().equals("")
				|| cbSindicato.getSelectedIndex() < 0) {
			JOptionPane.showMessageDialog(null, "Debe rellenar todos los campos");
			return;
		}
		PersistenceCRUDImpl<Chofer, Integer> choferDAO = new ChoferDAO();
		Chofer chofer = new Chofer();

		chofer.setNroCedula(cedula);
		chofer.setNombre(nombreChofer);
		chofer.setApellidos(apellidoChofer);

		chofer.setCodigoSindicato(codigoSindicato);
		chofer.setEstatus(estatusChofer);

		choferDAO.save(chofer);
		clean();
	}

	private void clean() {

		txtNombreSindicato.setText("");
		chckbxActivo.setSelected(false);

		txtNroCedula.setText("");
		txtNombre.setText("");
		txtApellido.setText("");
		cbSindicato.setSelectedIndex(-1);
		chEstatus.setSelected(false);

	}
}