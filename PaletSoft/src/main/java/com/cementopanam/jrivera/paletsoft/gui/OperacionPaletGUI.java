package com.cementopanam.jrivera.paletsoft.gui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

/**
 * @author jrivera
 *
 */
public class OperacionPaletGUI extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5487659675874867192L;
	private JTextField txtNumeroEntrega;
	private JTextField txtChofer1;
	private JTextField txtChofer2;
	private JTextField txtCodOperacion;
	private JTable table;

	/**
	 * Create the frame.
	 */
	public OperacionPaletGUI() {
		setIconifiable(true);
		setClosable(true);
		setTitle("Operacion Palet");
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(Color.GRAY));
		panel.setBounds(6, 22, 576, 300);
		getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblCodigoOperacion = new JLabel("Codigo Operacion");
		lblCodigoOperacion.setBounds(6, 12, 120, 16);
		panel.add(lblCodigoOperacion);
		lblCodigoOperacion.setFont(new Font("Verdana", Font.PLAIN, 12));

		JLabel lblCliente = new JLabel("Cliente");
		lblCliente.setBounds(6, 44, 55, 16);
		panel.add(lblCliente);
		lblCliente.setFont(new Font("Verdana", Font.PLAIN, 12));

		JLabel lblChofer = new JLabel("Chofer");
		lblChofer.setBounds(6, 79, 55, 16);
		panel.add(lblChofer);
		lblChofer.setFont(new Font("Verdana", Font.PLAIN, 12));

		JLabel lblNumeroEntregaSap = new JLabel("Numero Entrega SAP");
		lblNumeroEntregaSap.setBounds(6, 119, 141, 16);
		panel.add(lblNumeroEntregaSap);
		lblNumeroEntregaSap.setFont(new Font("Verdana", Font.PLAIN, 12));

		txtNumeroEntrega = new JTextField();
		txtNumeroEntrega.setBounds(159, 113, 168, 28);
		panel.add(txtNumeroEntrega);
		txtNumeroEntrega.setColumns(10);

		txtChofer1 = new JTextField();
		txtChofer1.setBounds(159, 73, 84, 28);
		panel.add(txtChofer1);
		txtChofer1.setColumns(10);

		txtChofer2 = new JTextField();
		txtChofer2.setBounds(255, 73, 168, 28);
		panel.add(txtChofer2);
		txtChofer2.setColumns(10);

		JComboBox cBCliente = new JComboBox();
		cBCliente.setBounds(159, 39, 264, 26);
		panel.add(cBCliente);

		txtCodOperacion = new JTextField();
		txtCodOperacion.setBounds(159, 6, 120, 28);
		panel.add(txtCodOperacion);
		txtCodOperacion.setColumns(10);

		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(435, 73, 90, 28);
		panel.add(btnBuscar);

		JButton button_1 = new JButton("+");
		button_1.setBounds(460, 113, 55, 28);
		panel.add(button_1);

		JButton button = new JButton("-");
		button.setBounds(515, 113, 55, 28);
		panel.add(button);

		JPanel panelTable = new JPanel();
		panelTable.setBackground(Color.GRAY);
		panelTable.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelTable.setBounds(6, 147, 564, 147);
		panel.add(panelTable);
		panelTable.setLayout(null);

		JScrollPane scrollPaneTable = new JScrollPane();
		scrollPaneTable.setBounds(0, 0, 564, 147);
		panelTable.add(scrollPaneTable);

		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Codigo Operacion", "Codigo Producto", "Cantidad PaletDAO" }));
		scrollPaneTable.setViewportView(table);

		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setFont(new Font("Verdana", Font.PLAIN, 12));
		btnGuardar.setBounds(390, 335, 90, 28);
		getContentPane().add(btnGuardar);

		JButton btnSalir = new JButton("Salir");
		btnSalir.setFont(new Font("Verdana", Font.PLAIN, 12));
		btnSalir.setBounds(492, 335, 90, 28);
		getContentPane().add(btnSalir);

	}
}
