package com.cementopanam.jrivera.paletsoft.dao;

import java.util.List;
import javax.naming.InitialContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class Almacen.
 * 
 * @see .Tblalmacen
 * @author Hibernate Tools
 */
public class AlmacenDAO {

	private static final Logger log = LogManager.getLogger(AlmacenDAO.class);
	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(AlmacenDAO transientInstance) {
		log.debug("persisting Almacen instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(AlmacenDAO instance) {
		log.debug("attaching dirty Almacen instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(AlmacenDAO instance) {
		log.debug("attaching clean Almacen instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(AlmacenDAO persistentInstance) {
		log.debug("deleting Almacen instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public AlmacenDAO merge(AlmacenDAO detachedInstance) {
		log.debug("merging Almacen instance");
		try {
			AlmacenDAO result = (AlmacenDAO) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public AlmacenDAO findById(java.lang.Integer id) {
		log.debug("getting Almacen instance with id: " + id);
		try {
			AlmacenDAO instance = (AlmacenDAO) sessionFactory.getCurrentSession().get("Almacen", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(AlmacenDAO instance) {
		log.debug("finding Almacen instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("Almacen").add(Example.create(instance))
					.list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
