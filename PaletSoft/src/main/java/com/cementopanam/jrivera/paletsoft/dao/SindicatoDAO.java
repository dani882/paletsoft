package com.cementopanam.jrivera.paletsoft.dao;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.cementopanam.jrivera.paletsoft.entidad.Sindicato;
import com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl;
import com.cementopanam.jrivera.paletsoft.util.HibernateUtil;

/**
 * Home object for domain model class Sindicato.
 * 
 * @see .Sindicato
 * @author Jesus Rivera
 */
public class SindicatoDAO implements PersistenceCRUDImpl<Sindicato, Integer> {

	private static final Logger LOGGER = LogManager.getLogger(SindicatoDAO.class);
	private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	/*
	 * Save the object in the database
	 * 
	 * @see
	 * com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl#save(java.lang.
	 * Object)
	 */
	@Override
	public void save(Sindicato sindicato) {

		LOGGER.info("persisting Sindicato instance");

		try (Session session = sessionFactory.openSession();) {

			session.getTransaction().begin();
			session.save(sindicato);
			session.getTransaction().commit();

		} catch (HibernateException hbe) {
			LOGGER.error(hbe.getMessage(), hbe);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Override
	public void update(Sindicato sindicato) {

		LOGGER.info("updating Sindicato instance");

		try (Session session = sessionFactory.openSession();) {

			session.getTransaction().begin();
			session.update(sindicato);
			session.getTransaction().commit();

		} catch (HibernateException hbe) {
			LOGGER.error(hbe.getMessage(), hbe);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Override
	public Sindicato findById(Integer id) {

		Session session = sessionFactory.openSession();
		Sindicato sindicato = session.get(Sindicato.class, id);

		return sindicato;
	}

	@Override
	public void delete(Sindicato sindicato) {

		LOGGER.info("deleting Sindicato instance");

		try (Session session = sessionFactory.openSession();) {

			Sindicato sin = findById(sindicato.getCodigoSindicato());
			// TODO Modificar para borrar mediante otros parametros ya que solo
			// borra por codigo
			session.getTransaction().begin();
			session.delete(sin);
			session.getTransaction().commit();

		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Sindicato> findAll() {

		List<Sindicato> sindicatos = null;
		Session session = null;
		DetachedCriteria criteria = null;

		try {
			session = sessionFactory.openSession();

			criteria = DetachedCriteria.forClass(Sindicato.class);
			sindicatos = criteria.getExecutableCriteria(session).list();

		}

		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			session.close();
		}

		return sindicatos;
	}

	/**
	 * Find the Sindicato ID by name
	 * 
	 * @param name
	 *            of the sindicato
	 * @return the sindicato ID
	 */
	public Integer findByName(String name) {

		Integer idSindicato = 0;
		Session session = null;
		DetachedCriteria criteria = null;
		try {
			session = sessionFactory.openSession();

			criteria = DetachedCriteria.forClass(Sindicato.class);
			criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
			criteria.setProjection(Projections.max("codigoSindicato"));
			criteria.add(Restrictions.eq("nombreSindicato", name));

			idSindicato = (Integer) criteria.getExecutableCriteria(session).list().get(0);
		}

		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			session.close();
		}

		return idSindicato;
	}
}