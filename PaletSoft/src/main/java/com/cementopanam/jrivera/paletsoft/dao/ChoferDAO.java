package com.cementopanam.jrivera.paletsoft.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;

import com.cementopanam.jrivera.paletsoft.entidad.Chofer;
import com.cementopanam.jrivera.paletsoft.entidad.Sindicato;
import com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl;
import com.cementopanam.jrivera.paletsoft.util.HibernateUtil;

/**
 * Home object for domain model class Chofer.
 * 
 * @see .Chofer
 * @author Jesus Rivera
 */
public class ChoferDAO implements PersistenceCRUDImpl<Chofer, Integer> {

    private static final Logger LOGGER = LogManager.getLogger(ChoferDAO.class);
    private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    /*
     * Save the object in the database
     * 
     * @see
     * com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl#save(java.lang.
     * Object)
     */
    @Override
    public void save(Chofer chofer) {

        LOGGER.info("persisting Chofer instance");

        try (Session session = sessionFactory.openSession();) {

            session.getTransaction().begin();
            session.save(chofer);
            session.getTransaction().commit();

        } catch (HibernateException hbe) {
            LOGGER.error(hbe.getMessage(), hbe);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Override
    public void update(Chofer chofer) {

        LOGGER.info("updating Chofer instance");

        try (Session session = sessionFactory.openSession();) {

            session.getTransaction().begin();
            session.update(chofer);
            session.getTransaction().commit();

        } catch (HibernateException hbe) {
            LOGGER.error(hbe.getMessage(), hbe);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Override
    public Chofer findById(Integer id) {

        Session session = sessionFactory.getCurrentSession();
        Chofer chofer = session.get(Chofer.class, id);
        session.close();

        return chofer;
    }

    @Override
    public void delete(Chofer chofer) {

        LOGGER.info("deleting Chofer instance");

        try (Session session = sessionFactory.openSession();) {

            Chofer ch = findById(chofer.getCodigoChofer());
            // TODO Modificar para borrar mediante otros parametros ya que solo
            // borra por codigo
            session.getTransaction().begin();
            session.delete(ch);
            session.getTransaction().commit();
        }
    }

    /*
     * Search for all Choferes
     * 
     * @see com.cementopanam.jrivera.paletsoft.util.PersistenceCRUDImpl#findAll()
     * 
     * @return List of Choferes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Chofer> findAll() {

        List<Chofer> choferes = null;
        Session session = null;
        DetachedCriteria criteria = null;

        try {
            session = sessionFactory.openSession();

            criteria = DetachedCriteria.forClass(Chofer.class);
            choferes = criteria.getExecutableCriteria(session).list();

        }

        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            session.close();
        }

        return choferes;
    }

    /**
     * Search for all Sindicatos
     * 
     * @return List of Sindicatos
     */

    public List<Sindicato> findAllSindicatos() {

        List<Sindicato> sindicatos = null;
        Session session = null;
        // DetachedCriteria criteria = null;
        CriteriaBuilder builder = null;
        CriteriaQuery<Sindicato> criteria = null;

        try {
            session = sessionFactory.openSession();
            // Create CriteriaBuilder
            builder = session.getCriteriaBuilder();
            // Create CriteriaQuery
            criteria = builder.createQuery(Sindicato.class);
            // Specify criteria root
            Root<Sindicato> sindicato = criteria.from(Sindicato.class);
            criteria.where(builder.equal(sindicato.get("estatus"), "Activo"));
            // Execute query
            sindicatos = session.createQuery(criteria).getResultList();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            session.close();
        }

        return sindicatos;

    }
}